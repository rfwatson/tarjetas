defmodule TarjetasWeb.Router do
  use TarjetasWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TarjetasWeb do
    pipe_through :browser # Use the default browser stack

    resources "/users", UserController, only: [:new, :create]
    resources "/session", SessionController, only: [:new, :create, :delete], singleton: true

    get "/", HomeController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", TarjetasWeb do
  #   pipe_through :api
  # end
end
