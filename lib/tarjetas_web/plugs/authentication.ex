defmodule TarjetasWeb.Plugs.Authentication do
  import Plug.Conn
  import Phoenix.Controller
  import TarjetasWeb.Router.Helpers

  def authenticate_user(conn, _) do
    case get_session(conn, :user_id) do
      nil ->
        conn
        |> put_flash(:error, "Please sign in to continue.")
        |> redirect(to: session_path(conn, :new))
        |> halt()
      user_id ->
        conn
        |> assign(:current_user, Tarjetas.Accounts.get_user!(user_id))
    end
  end
end
