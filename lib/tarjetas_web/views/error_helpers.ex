defmodule TarjetasWeb.ErrorHelpers do
  @moduledoc """
  Conveniences for translating and building error messages.
  """

  use Phoenix.HTML

  @doc """
  Generates input tag and errors for inlined form elements.
  """
  def input(form, field, label_text \\ nil) do
    type = Phoenix.HTML.Form.input_type(form, field)
    error = error_tag(form, field)
    label_text = label_text || humanize(field)
    input_opts = [class: form_input_class(error)]

    content_tag :div, class: "form-group" do
      label = label(form, field, label_text, class: "form-control-label")
      input = apply(Phoenix.HTML.Form, type, [form, field, input_opts])
      [label, input, error]
    end
  end

  defp form_input_class([] = _error), do: "form-control"
  defp form_input_class(_ = _error), do: "form-control is-invalid"

  @doc """
  Generates tag for inlined form input errors.
  """
  def error_tag(form, field) do
    Enum.map(Keyword.get_values(form.errors, field), fn (error) ->
      content_tag :div, translate_error(error), class: "form-control-feedback"
    end)
  end

  @doc """
  Translates an error message using gettext.
  """
  def translate_error({msg, opts}) do
    # When using gettext, we typically pass the strings we want
    # to translate as a static argument:
    #
    #     # Translate "is invalid" in the "errors" domain
    #     dgettext "errors", "is invalid"
    #
    #     # Translate the number of files with plural rules
    #     dngettext "errors", "1 file", "%{count} files", count
    #
    # Because the error messages we show in our forms and APIs
    # are defined inside Ecto, we need to translate them dynamically.
    # This requires us to call the Gettext module passing our gettext
    # backend as first argument.
    #
    # Note we use the "errors" domain, which means translations
    # should be written to the errors.po file. The :count option is
    # set by Ecto and indicates we should also apply plural rules.
    if count = opts[:count] do
      Gettext.dngettext(TarjetasWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(TarjetasWeb.Gettext, "errors", msg, opts)
    end
  end
end
