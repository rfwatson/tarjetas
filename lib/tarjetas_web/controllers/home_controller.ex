defmodule TarjetasWeb.HomeController do
  use TarjetasWeb, :controller

  import TarjetasWeb.Plugs.Authentication

  plug :authenticate_user

  def show(conn, _params) do
    render(conn, "show.html")
  end
end
