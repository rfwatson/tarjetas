defmodule TarjetasWeb.SessionController do
  use TarjetasWeb, :controller

  alias Tarjetas.Accounts

  def new(conn, _params) do
    render(conn, "new.html", conn: conn)
  end

  def create(conn, %{"username" => username, "password" => password}) do
    Accounts.authenticate(username, password)
    |> case do
      {:ok, user} ->
        create_session(conn, user)
      _ ->
        reject_session(conn)
    end
  end

  def delete(conn, _params) do
    conn
    |> configure_session(drop: true)
    |> redirect(to: home_path(conn, :show))
  end

  defp create_session(conn, user) do
    conn
    |> put_flash(:info, "Welcome back!")
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
    |> redirect(to: home_path(conn, :show))
  end

  defp reject_session(conn) do
    conn
    |> put_flash(:error, "That didn't work.")
    |> render("new.html", conn: conn)
  end
end
