defmodule Tarjetas.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  @minimum_username_length 2
  @maximum_username_length 50
  @minimum_password_length 8

  schema "users" do
    field :username, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_hash, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:username, :password, :password_confirmation])
    |> validate_required([:username, :password, :password_confirmation])
    |> validate_length(:username, min: @minimum_username_length, max: @maximum_username_length)
    |> validate_length(:password, min: @minimum_password_length)
    |> unique_constraint(:username)
    |> validate_passwords_match()
    |> hash_password()
    |> clear_plaintext_passwords()
  end

  defp validate_passwords_match(%{changes: %{password: password, password_confirmation: password_confirmation}} = changeset)
  when password == password_confirmation, do: changeset

  defp validate_passwords_match(changeset) do
    add_error(changeset, :password, "doesn't match confirmation")
  end

  defp hash_password(%{valid?: true, changes: %{password: password}} = changeset) do
    put_change(changeset, :password_hash, Comeonin.Argon2.hashpwsalt(password))
  end

  defp hash_password(changeset), do: changeset

  defp clear_plaintext_passwords(changeset) do
    change(changeset, %{password: nil, password_confirmation: nil})
  end
end
