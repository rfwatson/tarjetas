defmodule Tarjetas.Accounts do
  import Ecto.Query, warn: false
  alias Tarjetas.Repo

  alias Tarjetas.Accounts.User

  def get_user!(id), do: Repo.get!(User, id)

  def find_user_by_username(username)
  when is_binary(username) do
    from(u in User, where: u.username == ^username)
    |> Repo.one()
    |> case do
      %User{} = user ->
        {:ok, user}
      _ ->
        {:error, :not_found}
    end
  end

  def find_user_by_username(_username) do
    {:error, :not_found}
  end

  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def authenticate(%User{} = user, password) do
    Comeonin.Argon2.check_pass(user, password)
    |> case do
      {:ok, user} ->
        {:ok, user}
      _ ->
        {:error, :not_authorized}
    end
  end

  def authenticate(username, password)
  when is_binary(username) and is_binary(password)
  do
    case find_user_by_username(username) do
      {:ok, user} ->
        authenticate(user, password)
      _ ->
        {:error, :not_authorized}
    end
  end

  def authenticate(_username, _password) do
    {:error, :not_authorized}
  end
end
