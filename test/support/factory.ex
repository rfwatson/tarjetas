defmodule Tarjetas.Factory do
  use ExMachina.Ecto, repo: Tarjetas.Repo

  def new_user_factory do
    %Tarjetas.Accounts.User{
      username: sequence(:username, &"username#{&1}"),
      password: "testing123",
      password_confirmation: "testing123"
    }
  end

  def user_factory do
    %Tarjetas.Accounts.User{
      username: sequence(:username, &"username#{&1}"),
      password_hash: Comeonin.Argon2.hashpwsalt("testing123")
    }
  end
end
