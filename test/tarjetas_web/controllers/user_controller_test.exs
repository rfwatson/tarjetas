defmodule TarjetasWeb.UserControllerTest do
  use TarjetasWeb.ConnCase

  import Tarjetas.Factory

  describe "new/2" do
    test "it responds correctly", %{conn: conn} do
      conn = conn
      |> get(user_path(conn, :new))

      assert html_response(conn, 200)
    end
  end

  describe "create/2" do
    test "it responds correctly when creating a new user", %{conn: conn} do
      user = insert(:user)
      params = %{"username" => user.username, "password" => "testing123"}

      conn = conn
      |> post(session_path(conn, :create), params)

      assert redirected_to(conn) == home_path(conn, :show)
    end
  end
end
