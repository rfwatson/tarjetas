defmodule TarjetasWeb.HomeControllerTest do
  use TarjetasWeb.ConnCase

  import Tarjetas.Factory

  describe "new/2" do
    test "it redirects to the login page", %{conn: conn} do
      conn = get(conn, home_path(conn, :show))

      assert redirected_to(conn) == session_path(conn, :new)
    end

    test "when logged in, it renders 200 OK", %{conn: conn} do
      user = insert(:user)

      conn
      |> login_as(user)
      |> get(home_path(conn, :show))
      |> html_response(200)
    end
  end
end
