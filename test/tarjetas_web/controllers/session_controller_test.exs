defmodule TarjetasWeb.SessionControllerTest do
  use TarjetasWeb.ConnCase

  describe "new/2" do
    test "it responds correctly", %{conn: conn} do
      conn = conn
      |> get(session_path(conn, :new))

      assert html_response(conn, 200)
    end
  end

  describe "create/2" do
    test "it responds correctly", %{conn: conn} do
      conn = conn
      |> get(session_path(conn, :new))

      assert html_response(conn, 200)
    end
  end
end
