defmodule TarjetasWeb.Plugs.AuthenticationTest do
  use TarjetasWeb.ConnCase

  import Plug.Test
  import Tarjetas.Factory

  alias TarjetasWeb.Plugs.Authentication

  defp call_plug(conn, session_opts) do
    conn
    |> init_test_session(session_opts)
    |> fetch_flash()
    |> Authentication.authenticate_user(%{})
  end

  describe "authenticate_user" do
    test "when not logged in request is redirected", %{conn: conn} do
      conn = call_plug(conn, user_id: nil)

      assert redirected_to(conn) == session_path(conn, :new)
    end

    test "when not logged in request is halted", %{conn: conn} do
      conn = call_plug(conn, user_id: nil)

      assert conn.halted
    end

    test "when logged in request is not halted", %{conn: conn} do
      user = insert(:user)
      conn = call_plug(conn, user_id: user.id)

      refute conn.halted
    end

    test "when logged in the current user is assigned", %{conn: conn} do
      user = insert(:user)
      conn = call_plug(conn, user_id: user.id)

      assert conn.assigns.current_user == user
    end
  end
end
