defmodule Tarjetas.Accounts.UserTest do
  use Tarjetas.DataCase

  alias Tarjetas.Accounts.User

  import Tarjetas.Factory

  describe "default changeset" do
    test "it is invalid if the username is too short" do
      params = params_for(:new_user, username: "x")

      assert %{valid?: false, errors: [username: {msg, _}]} = User.changeset(%User{}, params)
      assert String.contains?(msg, "should be at least")
    end

    test "it is invalid if the username is too long" do
      params = params_for(:new_user, username: String.duplicate("x", 100))

      assert %{valid?: false, errors: [username: {msg, _}]} = User.changeset(%User{}, params)
      assert String.contains?(msg, "should be at most")
    end

    test "it is invalid if the password confirmation does not match" do
      params = params_for(:new_user, password: "abcdefgh", password_confirmation: "abcdefgj")

      assert %{valid?: false, errors: [password: {"doesn't match confirmation", _}]} = User.changeset(%User{}, params)
    end

    test "it is invalid if the password is too short" do
      params = params_for(:new_user, password: "x", password_confirmation: "x")

      assert %{valid?: false, errors: [password: {msg, _}]} = User.changeset(%User{}, params)
      assert String.contains?(msg, "should be at least")
    end
  end
end
