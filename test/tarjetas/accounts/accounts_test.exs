defmodule Tarjetas.AccountsTest do
  use Tarjetas.DataCase

  alias Tarjetas.Accounts
  import Tarjetas.Factory

  describe "users" do
    alias Tarjetas.Accounts.User

    def user_fixture(attrs \\ %{}) do
      {:ok, user} = params_for(:new_user, attrs)
      |> Accounts.create_user()

      user
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      valid_attrs = params_for(:new_user, username: "rob")
      {:ok, %User{} = user} = Accounts.create_user(valid_attrs)

      assert user.username == "rob"
    end

    test "create_user/1 with valid data hashes the password" do
      valid_attrs = params_for(:new_user)
      {:ok, %User{} = user} = Accounts.create_user(valid_attrs)

      refute is_nil(user.password_hash)
    end

    test "create_user/1 with invalid data returns error changeset" do
      invalid_attrs = params_for(:new_user, password: nil)
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(invalid_attrs)
    end

    test "create_user/1 does not allow a user with duplicate username to be inserted" do
      insert(:user, username: "rob")
      params = params_for(:new_user, %{username: "rob"})
      assert {:error, %{errors: [username: {"has already been taken", _}]}} = Accounts.create_user(params)
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end

    test "find_user_by_username/1 finds a user by username" do
      user = user_fixture()
      assert {:ok, user} = Accounts.find_user_by_username(user.username)
    end

    test "find_user_by_username/1 returns an error if the user cannot be found" do
      assert {:error, :not_found} = Accounts.find_user_by_username("somebody")
    end

    test "authenticate/2 with a username and password works in a successful case" do
      user = user_fixture()
      assert {:ok, user} = Accounts.authenticate(user.username, "testing123")
    end

    test "authenticate/2 with a username and password works in an unsuccessful case" do
      user = user_fixture()
      assert {:error, :not_authorized} = Accounts.authenticate(user.username, "nope")
    end

    test "authenticate/2 with a user struct and password works in a successful case" do
      user = user_fixture()
      assert {:ok, user} = Accounts.authenticate(user, "testing123")
    end

    test "authenticate/2 with a user struct and password works in an unsuccessful case" do
      user = user_fixture()
      assert {:error, :not_authorized} = Accounts.authenticate(user, "nope")
    end

    test "authenticate/2 with any other arguments returns a sensible result" do
      assert {:error, :not_authorized} = Accounts.authenticate(nil, "nope")
    end
  end
end
